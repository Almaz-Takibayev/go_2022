package mainService

import (
	"banking-system/database/handler"
	"banking-system/pkg/api"
	"context"
)

type UserServer struct{}

func (*UserServer) CreateUser(ctx context.Context, req *api.CreateUserRequest) (*api.UserResponse, error) {
	handler.CreateUser(handler.CreateUserInput{
		Name:    req.GetName(),
		Surname: req.GetSurname(),
		IIN:     req.GetIin(),
		Phone:   req.GetPhone(),
		BankID:  uint(req.GetBankId()),
	})
	return &api.UserResponse{}, nil
}

func (s *UserServer) GetAllUsers(ctx context.Context, req *api.BlankInput) (*api.UserListResponse, error) {
	users := handler.GetAllUsers()
	var x []*api.UserResponse
	for i := range users {
		user := users[i]
		convertedUser := &api.UserResponse{
			Id:      uint32(user.ID),
			Name:    user.Name,
			Surname: user.Surname,
			Iin:     user.IIN,
			Phone:   user.Phone,
			BankId:  uint32(user.BankID),
		}

		x = append(x, convertedUser)
	}
	return &api.UserListResponse{Users: x}, nil
}

func (s *UserServer) GetUser(ctx context.Context, req *api.GetUserRequest) (*api.UserResponse, error) {
	user := handler.GetUser(uint(req.GetId()))
	return &api.UserResponse{Id: uint32(user.ID), Name: user.Name, BankId: uint32(user.BankID), Surname: user.Surname, Iin: user.IIN, Phone: user.Phone}, nil
}
