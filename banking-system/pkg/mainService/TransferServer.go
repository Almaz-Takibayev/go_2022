package mainService

import (
	"banking-system/database/handler"
	"banking-system/pkg/api"
	"context"
)

type TransferServer struct{}

func (s *TransferServer) CreateTransfer(ctx context.Context, req *api.CreateTransferRequest) (*api.TransferResponse, error) {
	handler.CreateTransfer(handler.CreateTransferInput{BankId: uint(req.BankId), Sum: req.Sum, SenderBankingAccountID: uint(req.SenderBankingAccountID), TakerBankingAccountID: uint(req.TakerBankingAccountID)})
	return &api.TransferResponse{}, nil
}

func (s *TransferServer) GetAllTransfers(ctx context.Context, req *api.BlankInput) (*api.TransferListResponse, error) {
	transfers := handler.GetAllTransfers()
	var x []*api.TransferResponse
	for i := range transfers {
		transfer := transfers[i]
		convertedTransfer := &api.TransferResponse{
			Id:                     uint32(transfer.ID),
			Sum:                    transfer.Sum,
			BankId:                 uint32(transfer.BankID),
			SenderBankingAccountID: uint32(transfer.SenderBankingAccountID),
			TakerBankingAccountID:  uint32(transfer.TakerBankingAccountID),
		}

		x = append(x, convertedTransfer)
	}
	return &api.TransferListResponse{Transfers: x}, nil
}

func (s *TransferServer) GetTransfer(ctx context.Context, req *api.GetTransferRequest) (*api.TransferResponse, error) {
	transfer := handler.GetTransfer(uint(req.GetId()))
	return &api.TransferResponse{Id: uint32(transfer.ID), Sum: transfer.Sum, BankId: uint32(transfer.BankID), SenderBankingAccountID: uint32(transfer.SenderBankingAccountID), TakerBankingAccountID: uint32(transfer.TakerBankingAccountID)}, nil
}
