package database

import (
	model "banking-system/database/model"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var DB *gorm.DB

func ConnectDB() {
	connStr := "host = 127.0.0.1 port = 5432 user = postgres dbname = banking_system password = root sslmode = disable"
	db, err := gorm.Open("postgres", connStr)
	if err != nil {
		panic("Не удается подключится к базе")
	}

	db.AutoMigrate(&model.Bank{})
	db.AutoMigrate(&model.BankingAccount{})
	db.AutoMigrate(&model.Replenishment{})
	db.AutoMigrate(&model.Transfer{})
	db.AutoMigrate(&model.User{})
	db.AutoMigrate(&model.Withdrawal{})
	DB = db
}
