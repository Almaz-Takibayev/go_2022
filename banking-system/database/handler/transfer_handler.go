package handler

import (
	"banking-system/database"
	"banking-system/database/model"
	"fmt"
	"time"
)

type CreateTransferInput struct {
	Sum                    float32 `json:"sum" binding:"required"`
	BankId                 uint    `json:"bank_id" binding:"required"`
	SenderBankingAccountID uint    `json:"sender_banking_account_id" binding:"required"`
	TakerBankingAccountID  uint    `json:"taker_banking_account_id" binding:"required"`
}

func GetAllTransfers() []model.Transfer {
	var transfers []model.Transfer
	database.DB.Find(&transfers)
	return transfers
}

func GetTransfer(id uint) model.Transfer {
	var transfer model.Transfer

	if err := database.DB.Where("id=?", id).First(&transfer).Error; err != nil {
		fmt.Printf("Перевод с id=%v не найден!", id)
		return model.Transfer{}
	}

	return transfer
}

func CreateTransfer(input CreateTransferInput) {
	MinusBankingAccountBalance(input.SenderBankingAccountID, input.Sum)
	PlusBankingAccountBalance(input.TakerBankingAccountID, input.Sum)
	transfer := model.Transfer{Sum: input.Sum, BankID: input.BankId, SenderBankingAccountID: input.SenderBankingAccountID, TakerBankingAccountID: input.TakerBankingAccountID, Time: time.Now()}
	database.DB.Create(&transfer)
}
