package model

type BankingAccount struct {
	ID             uint    `json:"id" gorm:"primary_key"`
	Balance        float32 `json:"balance"`
	BankID         uint    `json:"BankID" gorm:"<-:create"`
	UserID         uint    `json:"UserID" gorm:"<-:create"`
	Replenishments []Replenishment
	Withdrawals    []Withdrawal
	SentTransfers  []Transfer
	TakenTransfers []Transfer
}
