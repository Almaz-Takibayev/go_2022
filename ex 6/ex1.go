package main

import (
	"fmt"
	"time"
)

func a(channel chan bool) {
	for i := 0; i < 50; i++ {
		fmt.Print("a")
	}
	channel <- true
}

func b(channel chan bool) {
	for i := 0; i < 50; i++ {
		fmt.Print("b")
	}
	channel <- false
}

func main() {
	channel := make(chan bool)
	go a(channel)
	go b(channel)
	time.Sleep(time.Second)
	fmt.Println(<-channel)
	fmt.Println(<-channel)
	fmt.Println("end main()")
}
