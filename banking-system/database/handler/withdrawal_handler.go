package handler

import (
	"banking-system/database"
	"banking-system/database/model"
	"fmt"
	"time"
)

type CreateWithdrawalInput struct {
	Sum              float32 `json:"sum" binding:"required"`
	BankId           uint    `json:"bank_id" binding:"required"`
	BankingAccountID uint    `json:"banking_account_id" binding:"required"`
}

func GetAllWithdrawal() []model.Withdrawal {
	var withdrawals []model.Withdrawal
	database.DB.Find(&withdrawals)
	return withdrawals
}

func GetWithdrawal(id uint) model.Withdrawal {
	var withdrawal model.Withdrawal

	if err := database.DB.Where("id=?", id).First(&withdrawal).Error; err != nil {
		fmt.Printf("Снятие с id=%v не найдено!", id)
		return model.Withdrawal{}
	}

	return withdrawal
}

func CreateWithdrawal(input CreateWithdrawalInput) {
	MinusBankingAccountBalance(input.BankingAccountID, input.Sum)
	withdrawal := model.Withdrawal{Sum: input.Sum, BankID: input.BankId, BankingAccountID: input.BankingAccountID, Time: time.Now()}
	database.DB.Create(&withdrawal)
}
