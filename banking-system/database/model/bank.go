package model

type Bank struct {
	ID              uint   `json:"id" gorm:"primary_key"`
	Name            string `json:"name" gorm:"unique"`
	Users           []User
	BankingAccounts []BankingAccount
	Transfers       []Transfer
	Replenishments  []Replenishment
	Withdrawals     []Withdrawal
}
