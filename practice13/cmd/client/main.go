package main

import (
	"context"
	"flag"
	"google.golang.org/grpc"
	"log"
	"practice13/pkg/api"
)

func main() {
	flag.Parse()
	if flag.NArg() < 1 {
		log.Fatal("not enough arg")
	}

	text := flag.Arg(0)

	conn, err := grpc.Dial(":8080", grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}

	client := api.NewReverserClient(conn)

	res, err := client.Reverse(context.Background(), &api.ReverseRequest{Text: text})
	if err != nil {
		log.Fatal(err)
	}

	log.Println(res.GetResult())
}
