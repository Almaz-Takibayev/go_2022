package reverser

import (
	"context"
	"practice13/pkg/api"
)

type GRPCServer struct{}

func (s *GRPCServer) Reverse(ctx context.Context, req *api.ReverseRequest) (*api.ReverseResponse, error) {
	var result string
	for _, letter := range req.Text {
		result = string(letter) + result
	}
	return &api.ReverseResponse{
		Result: result,
	}, nil
}
