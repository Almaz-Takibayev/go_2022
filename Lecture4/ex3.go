package main

import (
	"errors"
	"fmt"
	"log"
)

func main() {
	perimeter, err := perimeter(-1, 10.0)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Perimeter is", perimeter)
}

func perimeter(length float64, width float64) (float64, error) {
	if length < 0 {
		return 0, errors.New(fmt.Sprintf("a length of %f is invalid", length))
	}

	if width < 0 {
		return 0, errors.New(fmt.Sprintf("a width of %f is invalid", width))
	}

	return 2 * (length + width), nil
}
