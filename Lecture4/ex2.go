package main

import "fmt"

func main() {
	fmt.Println("Perimeter of the plots of land above is", perimeter(8.2, 10.0)+perimeter(5.0, 5.2)+perimeter(6.2, 4.5))
}

func perimeter(length float64, width float64) float64 {
	return 2 * (length + width)
}
