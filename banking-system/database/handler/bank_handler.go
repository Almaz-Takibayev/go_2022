package handler

import (
	"banking-system/database"
	"banking-system/database/model"
	"fmt"
)

type CreateBankInput struct {
	Name string `json:"name" binding:"required"`
}

func GetAllBanks() []model.Bank {
	var banks []model.Bank
	database.DB.Find(&banks)
	return banks
}

func GetBank(id uint) model.Bank {
	var bank model.Bank

	if err := database.DB.Where("id=?", id).First(&bank).Error; err != nil {
		fmt.Printf("Банк с id=%v не найден!", id)
		return model.Bank{}
	}

	return bank
}

func CreateBank(input CreateBankInput) {
	bank := model.Bank{Name: input.Name}
	database.DB.Create(&bank)
}
