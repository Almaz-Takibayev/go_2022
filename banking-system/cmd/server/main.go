package main

import (
	"banking-system/database"
	"banking-system/pkg/api"
	"banking-system/pkg/mainService"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	database.ConnectDB()
	s := grpc.NewServer()
	bankServer := &mainService.BankServer{}
	api.RegisterBankServer(s, bankServer)
	userServer := &mainService.UserServer{}
	api.RegisterUserServer(s, userServer)
	bankingAccountServer := &mainService.BankingAccountServer{}
	api.RegisterBankingAccountServer(s, bankingAccountServer)
	replenishmentServer := &mainService.ReplenishmentServer{}
	api.RegisterReplenishmentServer(s, replenishmentServer)
	transferServer := &mainService.TransferServer{}
	api.RegisterTransferServer(s, transferServer)
	withdrawalServer := &mainService.WithdrawalServer{}
	api.RegisterWithdrawalServer(s, withdrawalServer)
	l, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal(err)
	}
	if err := s.Serve(l); err != nil {
		log.Fatal(err)
	}
}
