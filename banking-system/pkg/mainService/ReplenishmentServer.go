package mainService

import (
	"banking-system/database/handler"
	"banking-system/pkg/api"
	"context"
)

type ReplenishmentServer struct{}

func (s *ReplenishmentServer) CreateReplenishment(ctx context.Context, req *api.CreateReplenishmentRequest) (*api.ReplenishmentResponse, error) {
	handler.CreateReplenishment(handler.CreateReplenishmentInput{BankId: uint(req.BankId), Sum: req.Sum, BankingAccountID: uint(req.BankingAccountId)})
	return &api.ReplenishmentResponse{}, nil
}

func (s *ReplenishmentServer) GetAllReplenishments(ctx context.Context, req *api.BlankInput) (*api.ReplenishmentListResponse, error) {
	replenishments := handler.GetAllReplenishments()
	var x []*api.ReplenishmentResponse
	for i := range replenishments {
		replenishment := replenishments[i]
		convertedReplenishment := &api.ReplenishmentResponse{
			Id:               uint32(replenishment.ID),
			Sum:              replenishment.Sum,
			BankId:           uint32(replenishment.BankID),
			BankingAccountId: uint32(replenishment.BankingAccountID),
		}

		x = append(x, convertedReplenishment)
	}
	return &api.ReplenishmentListResponse{Replenishments: x}, nil
}

func (s *ReplenishmentServer) GetReplenishment(ctx context.Context, req *api.GetReplenishmentRequest) (*api.ReplenishmentResponse, error) {
	replenishment := handler.GetReplenishment(uint(req.GetId()))
	return &api.ReplenishmentResponse{Id: uint32(replenishment.ID), Sum: replenishment.Sum, BankId: uint32(replenishment.BankID), BankingAccountId: uint32(replenishment.BankingAccountID)}, nil
}
