package model

type User struct {
	ID             uint   `json:"id" gorm:"primary_key"`
	Name           string `json:"name"`
	Surname        string `json:"surname"`
	IIN            string `json:"iin" gorm:"unique"`
	Phone          string `json:"phone" gorm:"unique"`
	BankID         uint   `json:"bankID"`
	BankingAccount BankingAccount
}
