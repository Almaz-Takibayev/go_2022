package main

import (
	"fmt"
)

func main() {
	fmt.Println("     Name |  Score 1 |  Score 2 |  Score 3 |  Average")
	fmt.Println("_____________________________________________________")
	scoreSummary("Jermaine", 95.40, 82.30, 74.60)
	scoreSummary("Jessie", 95.40, 82.30, 74.60)
	scoreSummary("Lamar", 95.40, 82.30, 74.60)
}

func scoreSummary(name string, score1 float64, score2 float64, score3 float64) {
	for i := 0; i < 9-len(name); i++ {
		fmt.Print(" ")
	}
	fmt.Println(name, "|    ", score1, "|    ", score2, "|    ", score3, "|    ", (score1+score2+score3)/3)
}
