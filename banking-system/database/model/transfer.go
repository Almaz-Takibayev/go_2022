package model

import "time"

type Transfer struct {
	ID                     uint      `json:"id" gorm:"primary_key"`
	BankID                 uint      `json:"bankID" gorm:"<-:create"`
	Sum                    float32   `json:"sum" gorm:"<-:create"`
	Time                   time.Time `json:"time" gorm:"<-:create"`
	SenderBankingAccountID uint      `json:"senderBankingAccountID" gorm:"<-:create"`
	TakerBankingAccountID  uint      `json:"takerBankingAccountID" gorm:"<-:create"`
}
