package mainService

import (
	"banking-system/database/handler"
	"banking-system/pkg/api"
	"context"
)

type BankingAccountServer struct{}

func (s *BankingAccountServer) CreateBankingAccount(ctx context.Context, req *api.CreateBankingAccountRequest) (*api.BankingAccountResponse, error) {
	handler.CreateBankingAccount(handler.CreateBankingAccountInput{BankId: uint(req.BankId), Balance: req.Balance, UserId: uint(req.UserId)})
	return &api.BankingAccountResponse{}, nil
}

func (s *BankingAccountServer) GetAllBankingAccounts(ctx context.Context, req *api.BlankInput) (*api.BankingAccountListResponse, error) {
	bankingAccounts := handler.GetAllBankingAccounts()
	var x []*api.BankingAccountResponse
	for i := range bankingAccounts {
		bankingAccount := bankingAccounts[i]
		convertedBankingAccount := &api.BankingAccountResponse{
			Id:      uint32(bankingAccount.ID),
			Balance: bankingAccount.Balance,
			BankId:  uint32(bankingAccount.BankID),
			UserId:  uint32(bankingAccount.UserID),
		}

		x = append(x, convertedBankingAccount)
	}
	return &api.BankingAccountListResponse{BankingAccounts: x}, nil
}

func (s *BankingAccountServer) GetBankingAccount(ctx context.Context, req *api.GetBankingAccountRequest) (*api.BankingAccountResponse, error) {
	bankingAccount := handler.GetBankingAccount(uint(req.GetId()))
	return &api.BankingAccountResponse{Id: uint32(bankingAccount.ID), Balance: bankingAccount.Balance, BankId: uint32(bankingAccount.BankID), UserId: uint32(bankingAccount.UserID)}, nil
}
