package mainService

import (
	"banking-system/database/handler"
	"banking-system/pkg/api"
	"context"
)

type BankServer struct{}

func (s *BankServer) CreateBank(ctx context.Context, req *api.CreateBankRequest) (*api.BankResponse, error) {
	handler.CreateBank(handler.CreateBankInput{Name: req.Name})
	return &api.BankResponse{}, nil
}

func (s *BankServer) GetAllBanks(ctx context.Context, req *api.BlankInput) (*api.BankListResponse, error) {
	banks := handler.GetAllBanks()
	var x []*api.BankResponse
	for i := range banks {
		bank := banks[i]
		convertedBank := &api.BankResponse{
			Id:   uint32(bank.ID),
			Name: bank.Name,
		}

		x = append(x, convertedBank)
	}
	return &api.BankListResponse{Banks: x}, nil
}

func (s *BankServer) GetBank(ctx context.Context, req *api.GetBankRequest) (*api.BankResponse, error) {
	bank := handler.GetBank(uint(req.GetId()))
	return &api.BankResponse{Id: uint32(bank.ID), Name: bank.Name}, nil
}
