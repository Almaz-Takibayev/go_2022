package handler

import (
	"banking-system/database"
	"banking-system/database/model"
	"fmt"
)

type CreateBankingAccountInput struct {
	Balance float32 `json:"balance" binding:"required"`
	BankId  uint    `json:"bank_id" binding:"required"`
	UserId  uint    `json:"user_id" binding:"required"`
}

func GetAllBankingAccounts() []model.BankingAccount {
	var bankingAccounts []model.BankingAccount
	database.DB.Find(&bankingAccounts)
	return bankingAccounts
}

func GetBankingAccount(id uint) model.BankingAccount {
	var bankingAccount model.BankingAccount

	if err := database.DB.Where("id=?", id).First(&bankingAccount).Error; err != nil {
		fmt.Printf("Банковский счет с id=%v не найден!", id)
		return model.BankingAccount{}
	}

	return bankingAccount
}

func CreateBankingAccount(input CreateBankingAccountInput) {
	bankingAccount := model.BankingAccount{Balance: input.Balance, BankID: input.BankId, UserID: input.UserId}
	database.DB.Create(&bankingAccount)
}

func MinusBankingAccountBalance(id uint, sum float32) {
	var bankingAccount model.BankingAccount

	if err := database.DB.Where("id=?", id).First(&bankingAccount).Error; err != nil {
		fmt.Printf("Банковский счет с id=%v не найден!", id)
		return
	}

	updatedAccount := bankingAccount
	if updatedAccount.Balance >= sum {
		updatedAccount.Balance = updatedAccount.Balance - sum
	} else {
		fmt.Printf("Недостаточно средств!")
		return
	}

	database.DB.Model(&bankingAccount).Update(updatedAccount)
}

func PlusBankingAccountBalance(id uint, sum float32) {
	var bankingAccount model.BankingAccount

	if err := database.DB.Where("id=?", id).First(&bankingAccount).Error; err != nil {
		fmt.Printf("Банковский счет с id=%v не найден!", id)
		return
	}

	updatedAccount := bankingAccount
	updatedAccount.Balance = updatedAccount.Balance + sum

	database.DB.Model(&bankingAccount).Update(updatedAccount)
}
