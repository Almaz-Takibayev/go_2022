package model

import "time"

// Withdrawal -> снятие
type Withdrawal struct {
	ID               uint      `json:"id" gorm:"primary_key"`
	BankID           uint      `json:"bankID" gorm:"<-:create"`
	Sum              float32   `json:"sum" gorm:"<-:create"`
	Time             time.Time `json:"time" gorm:"<-:create"`
	BankingAccountID uint      `json:"bankingAccountID" gorm:"<-:create"`
}
