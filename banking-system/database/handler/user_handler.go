package handler

import (
	"banking-system/database"
	"banking-system/database/model"
	"fmt"
)

type CreateUserInput struct {
	Name    string `json:"name" binding:"required"`
	Surname string `json:"surname" binding:"required"`
	IIN     string `json:"iin" binding:"required"`
	Phone   string `json:"phone" binding:"required"`
	BankID  uint   `json:"bank_id" binding:"required"`
}

func GetAllUsers() []model.User {
	var users []model.User
	database.DB.Find(&users)
	return users
}

func GetUser(id uint) model.User {
	var user model.User

	if err := database.DB.Where("id=?", id).First(&user).Error; err != nil {
		fmt.Printf("Пользователь с id=%v не найден!", id)
		return model.User{}
	}

	return user
}

func CreateUser(input CreateUserInput) {
	user := model.User{Name: input.Name}
	database.DB.Create(&user)
}
