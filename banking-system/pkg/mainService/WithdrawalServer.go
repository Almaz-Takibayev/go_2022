package mainService

import (
	"banking-system/database/handler"
	"banking-system/pkg/api"
	"context"
)

type WithdrawalServer struct{}

func (s *WithdrawalServer) CreateWithdrawal(ctx context.Context, req *api.CreateWithdrawalRequest) (*api.WithdrawalResponse, error) {
	handler.CreateWithdrawal(handler.CreateWithdrawalInput{BankId: uint(req.BankId), Sum: req.Sum, BankingAccountID: uint(req.BankingAccountId)})
	return &api.WithdrawalResponse{}, nil
}

func (s *WithdrawalServer) GetAllWithdrawals(ctx context.Context, req *api.BlankInput) (*api.WithdrawalListResponse, error) {
	withdrawals := handler.GetAllWithdrawal()
	var x []*api.WithdrawalResponse
	for i := range withdrawals {
		withdrawal := withdrawals[i]
		convertedWithdrawal := &api.WithdrawalResponse{
			Id:               uint32(withdrawal.ID),
			Sum:              withdrawal.Sum,
			BankId:           uint32(withdrawal.BankID),
			BankingAccountId: uint32(withdrawal.BankingAccountID),
		}

		x = append(x, convertedWithdrawal)
	}
	return &api.WithdrawalListResponse{Withdrawals: x}, nil
}

func (s *WithdrawalServer) GetWithdrawal(ctx context.Context, req *api.GetWithdrawalRequest) (*api.WithdrawalResponse, error) {
	withdrawal := handler.GetWithdrawal(uint(req.GetId()))
	return &api.WithdrawalResponse{Id: uint32(withdrawal.ID), Sum: withdrawal.Sum, BankId: uint32(withdrawal.BankID), BankingAccountId: uint32(withdrawal.BankingAccountID)}, nil
}
