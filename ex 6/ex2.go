package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	var wg sync.WaitGroup
	// размер группы (количество go routines)
	wg.Add(2)
	// анонимная функция
	work := func(id int) {
		// останавливаем
		defer wg.Done()
		fmt.Printf("Goroutines %d start work\n", id)
		time.Sleep(time.Second * 2)
		fmt.Printf("Goroutines %d start work\n", id)
	}

	// вызываем go routine
	// с каждым вызовом размер группы будем уменьшаться и будет вызываться следующая горутина
	go work(1)
	go work(2)

	// ожидаем группу
	wg.Wait()
	fmt.Println("Goroutines done work")
}
