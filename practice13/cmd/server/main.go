package main

import (
	"google.golang.org/grpc"
	"log"
	"net"
	"practice13/pkg/api"
	"practice13/pkg/reverser"
)

func main() {
	s := grpc.NewServer()
	srv := &reverser.GRPCServer{}
	api.RegisterReverserServer(s, srv)
	l, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal(err)
	}
	if err := s.Serve(l); err != nil {
		log.Fatal(err)
	}
}
