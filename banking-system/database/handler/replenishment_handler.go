package handler

import (
	"banking-system/database"
	"banking-system/database/model"
	"fmt"
	"time"
)

type CreateReplenishmentInput struct {
	Sum              float32 `json:"sum" binding:"required"`
	BankId           uint    `json:"bank_id" binding:"required"`
	BankingAccountID uint    `json:"banking_account_id" binding:"required"`
}

func GetAllReplenishments() []model.Replenishment {
	var replenishments []model.Replenishment
	database.DB.Find(&replenishments)
	return replenishments
}

func GetReplenishment(id uint) model.Replenishment {
	var replenishment model.Replenishment

	if err := database.DB.Where("id=?", id).First(&replenishment).Error; err != nil {
		fmt.Printf("Пополнение с id=%v не найдено!", id)
		return model.Replenishment{}
	}

	return replenishment
}

func CreateReplenishment(input CreateReplenishmentInput) {
	PlusBankingAccountBalance(input.BankingAccountID, input.Sum)
	replenishment := model.Replenishment{Sum: input.Sum, BankID: input.BankId, BankingAccountID: input.BankingAccountID, Time: time.Now()}
	database.DB.Create(&replenishment)
}
